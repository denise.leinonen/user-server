package com.example.demo.users;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UsersService {
    WebClient.Builder webClient;

    public List<UsersEntity> usersEntitiesList = new ArrayList<>();

    public UsersEntity addUser(User user) {
        UsersEntity usersEntity = new UsersEntity(
                UUID.randomUUID().toString(),
                user.getUsername(),
                user.getLastname(),
                user.getGroups());
        usersEntitiesList.add(usersEntity);
        return usersEntity;
    }

    public UsersEntity getById(String id) {
        UsersEntity usersEntity = new UsersEntity();
        for(UsersEntity usersEntity1 : usersEntitiesList){
            if(usersEntity1.getId().equals(id)){
                usersEntity = usersEntity1;
            }
        }
        return usersEntity;
    }

    public List<UsersEntity> deleteUserById(String id){
        UsersEntity usersEntity = getById(id);
        usersEntitiesList.remove(usersEntity);
        return usersEntitiesList;
    }

    public List<UsersEntity> getAllUsers() {
        for(int i = 0; i < usersEntitiesList.size(); i++){
            return usersEntitiesList;
        }
        return null;
    }
    public UsersEntity addGroupToUser(String id, String groupId) {
        UsersEntity usersEntity = getById(id);
        usersEntity.groups.add(groupId);
        return usersEntity;
    }

    /**************************************Webbanrop********************************************/

    public Group addUserToGroup (String id, String groupId){
        WebClient webClient = WebClient.create();

           return  webClient
                .put()
                .uri("http://localhost:8081/groups/update_group_list/" + id + "/" + groupId)
                .retrieve()
                .bodyToMono(Group.class)
                .block();
    }
    public List<Group> getAllGroups(){
        WebClient webClient = WebClient.create();

        return webClient
                .get()
                .uri("http://localhost:8081/groups/get_all_groups")
                .retrieve()
                .bodyToFlux(Group.class)
                .collectList()
                .block();
    }
    public Group getGroupById(String groupId){
        WebClient webClient = WebClient.create();

        return webClient
                .get()
                .uri("http://localhost:8081/groups/get_group/" + groupId)
                .retrieve()
                .bodyToMono(Group.class)
                .block();
    }
    public List<Group> deleteGroupById(String groupId){
        WebClient webClient = WebClient.create();

        return webClient
                .delete()
                .uri("http://localhost:8081/groups/delete_group/" + groupId)
                .retrieve()
                .bodyToFlux(Group.class)
                .collectList()
                .block();
    }
}
