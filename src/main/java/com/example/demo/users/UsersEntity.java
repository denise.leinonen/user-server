package com.example.demo.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UsersEntity {
    String id;
    String username;
    String lastname;
    List<String> groups;

    @JsonCreator
    public UsersEntity(@JsonProperty("id") String id,
                       @JsonProperty("username") String username,
                       @JsonProperty("lastname") String lastname,
                       @JsonProperty("groups") List<String> groups) {
        this.id = id;
        this.username = username;
        this.lastname = lastname;
        this.groups = groups;
    }
}
