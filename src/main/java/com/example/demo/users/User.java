package com.example.demo.users;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.List;

@Builder(toBuilder = true)
@Getter
@Setter
public class User {
    String username;
    String lastname;
    List<String> groups;

    public User(String username, String lastname, List<String> groups) {
        this.username = username;
        this.lastname = lastname;
        this.groups = groups;
    }
}
