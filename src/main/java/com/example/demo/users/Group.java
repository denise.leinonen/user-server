package com.example.demo.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
public class Group {
    String id;
    String name;
    List<String> users;

    @JsonCreator
    public Group(@JsonProperty("id")String id,
                       @JsonProperty ("name") String name,
                       @JsonProperty ("users") List<String> users) {
        this.id = id;
        this.name = name;
        this.users = users;
    }
}
