package com.example.demo.users;

import org.springframework.web.bind.annotation.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "/users")
public class UsersController {
    UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }
    @PostMapping("/add")
    public UsersEntity addUser(@RequestBody User user){
        return usersService.addUser(user);
    }
    @GetMapping("/Get_user{id}")
    public UsersEntity getUserById(@PathVariable("id") String id){
        return usersService.getById(id);
    }
    @GetMapping
    public List<UsersEntity> getAll(){
        return usersService.getAllUsers();
    }
    @DeleteMapping("/Delete_User_By_/{id}")
    public List<UsersEntity> deleteUserById(@PathVariable("id") String id){
        return usersService.deleteUserById(id);
    }
    @PutMapping("/add_user_to_group")
    public Group addUserToGroup(@RequestParam String id, @RequestParam String groupId){
        return usersService.addUserToGroup(id, groupId);
    }
    @GetMapping("/get_all_groups")
    public List<Group> getAllGroups(){
        return usersService.getAllGroups();
    }
    @PutMapping("/add_group_to_user")
    public UsersEntity addGroupToUser(@RequestParam String id, @RequestParam String groupId){
        return usersService.addGroupToUser(id, groupId);
    }
    @GetMapping("/get_group_by_id")
    public Group getGroupById(@RequestParam String groupId){
        return usersService.getGroupById(groupId);
    }
    @DeleteMapping("/delete_group_by_id")
    public List<Group> deleteGroupById(@RequestParam String groupId){
        return usersService.deleteGroupById(groupId);
    }
}
